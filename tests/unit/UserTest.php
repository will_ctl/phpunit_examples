<?php

use \PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    protected $user;

    // this will run before each test
    public function setUp() {
        $this->user = new \App\Models\User;
    }

    /** @test */
    public function that_we_can_get_the_first_Mame() {
        $this->user->setFirstName('Billy');

        $this->assertEquals($this->user->getFirstName(), 'Billy');
    }

    /** @test */
    public function that_we_can_get_the_last_name() {
        $this->user->setLastName('Garrett');

        $this->assertEquals($this->user->getLastName(), 'Garrett');
    }

    /** @test */
    public function testFullNameIsReturned() {
        $this->user->setFirstName('Billy');
        $this->user->setLastName('Garrett');

        $this->assertEquals($this->user->getFullName(), 'Billy Garrett');
    }

    /** @test */
    public function testFirstAndLastNameAreTrimmed() {
        $this->user->setFirstName('  Billy  ');
        $this->user->setLastName('   Garrett');

        $this->assertEquals($this->user->getFirstName(), 'Billy');
        $this->assertEquals($this->user->getLastName(), 'Garrett');
    }

    /** @test */
    public function testEmailAddressCanBeSet() {
        $this->user->setEmail('billy@codecourse.com');

        $this->assertEquals($this->user->getEmail(), 'billy@codecourse.com');
    }

    /** @test */
    public function testEmailVariablesContainCorrectValues() {
        $this->user->setFirstName('Billy');
        $this->user->setLastName('Garrett');
        $this->user->setEmail('billy@codecourse.com');

        $emailVariables = $this->user->getEmailVariables();

        $this->assertArrayHasKey('full_name', $emailVariables);
        $this->assertArrayHasKey('email', $emailVariables);

        $this->assertEquals($emailVariables['full_name'], 'Billy Garrett');
        $this->assertEquals($emailVariables['email'], 'billy@codecourse.com');
    }
}
