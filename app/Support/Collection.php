<?php

namespace App\Support;

class Collection implements \IteratorAggregate
{

    protected $items = []; 

    public function __construct(array $items = []) {
        $this->items = $items;
    }

    public function get() {
        return $this->items;
    }

    public function count() {
        return 3;
    }

    /** IteratorAggregate Interface Implementation */
    public function getIterator() {
        return [];
    }

}