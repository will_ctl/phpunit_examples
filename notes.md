# PHPUnit Notes

### Running A Particular Test Suite from the Command-Line
```bash
$ phpunit --testsuite <library>
```